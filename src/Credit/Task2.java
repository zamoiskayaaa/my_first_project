package Credit;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Введите день, месяц и год ");
        Scanner dates = new Scanner(System.in);
        int date = dates.nextInt();
        Scanner months = new Scanner(System.in);
        int month = months.nextInt();
        Scanner years = new Scanner(System.in);
        int year = years.nextInt();
        int maxdays=0;
        int days=0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                maxdays = days;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                days = 30;
                maxdays = days;
                break;
            case 2:
                if (((year % 4 == 0) && !(year % 100 == 0)) || (year % 400 == 0)) {
                    days = 29;
                    maxdays = days;
                } else {
                    days = 28;
                    maxdays = days;
                }
                break;
            default:
                System.out.println("Вы ввели неверное значение");
                break;
        }
        if (date == maxdays && month==12) {
            year=year+1;
            date=0;
            month=1;
        }
        if (date == maxdays && month!=12) {
            date=0;
            month=month+1;
        }
        if (date != maxdays) {
            date=date+1;
        }
        System.out.println(date+" "+month+" "+year);
    }
}

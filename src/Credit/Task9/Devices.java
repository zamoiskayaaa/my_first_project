package Credit.Task9;

public class Devices {
    public String name;
    public int weight;
    public int power;
    public boolean socket;

    public Devices(String name, int weight, int power, boolean socket) {
        this.name = name;
        this.weight = weight;
        this.power = power;
        this.socket = socket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPower() {
        return power;
    }

    public void setPower() {
        this.power = power;
    }

    public boolean isSocket() {
        return socket;
    }

    public void setSocket(boolean socket) {
        this.socket = socket;
    }

    @Override
    public String toString() {
        return "Devices{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", power=" + power +
                ", socket=" + socket +
                '}';
    }
}


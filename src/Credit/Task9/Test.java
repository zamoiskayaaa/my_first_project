package Credit.Task9;

import java.util.Scanner;
import java.util.TreeSet;

//сортировка по парметру power
public class Test {
    public static void main(String[] args) {
        Devices tv = new Devices("телевизор", 25, 180, false);
        Devices computer = new Devices("компьютер", 10, 220, true);
        Devices lamp = new Devices("лампа", 2, 120, true);
        DevicesPowerComporator comp = new DevicesPowerComporator();
        TreeSet<Devices> devices = new TreeSet<Devices>(comp);
        devices.add(tv);
        devices.add(computer);
        devices.add(lamp);
        int totalPower = 0;
        for (Devices j : devices) {
            if (j.isSocket()) {
                totalPower += j.getPower();
            }
        }
        System.out.println("Общая потребляемая мощность = " + totalPower);
        System.out.println();
        for (Devices i : devices) {
            System.out.println(i);
        }
        System.out.println("Введите минимальный вес");
        Scanner scannerWeightMin = new Scanner(System.in);
        int userWeightMin = scannerWeightMin.nextInt();
        System.out.println("Введите максимальный вес");
        Scanner scannerWeightMax = new Scanner(System.in);
        int userWeighMax = scannerWeightMin.nextInt();
        System.out.println("Введите мощность минимальную мощность");
        Scanner scannerPowerMin = new Scanner(System.in);
        int userPowerMin = scannerWeightMin.nextInt();
        System.out.println("Введите мощность максимальную мощность");
        Scanner scannerPowerMax = new Scanner(System.in);
        int userPowerMax = scannerWeightMin.nextInt();
        for (Devices d : devices) {
            if (d.getWeight() >= userWeightMin && d.getWeight() <= userWeighMax &&
                    d.getPower() >= userPowerMin && d.getPower() <= userPowerMax) {
                System.out.println(d);
            } else {
//при выходе параметров из диапазона не выводит ничего
            }
        }
    }
}

package Credit.Task9;

import java.util.Comparator;

public class DevicesPowerComporator implements Comparator <Devices> {

@Override
    public int compare(Devices o1, Devices o2) {
        if (o1.getPower() > o2.getPower()) {
            return 1;
        } else {
            if (o1.getPower() < o2.getPower()) {
                return -1;
            }else{
                return 0;
            }

        }
    }
}

package Credit;

public class Task5 {

    public static void main(String[] args) {
        int b = 0;
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 60; j++) {
                if ((j / 10 % 10) == (i % 10) && (i / 10 % 10) == (j % 10)) {
                    b++;
                }
            }
        }
        System.out.println(b + " раз за сутки случается так, что слева от двоеточия показывается симметричная комбинация ");
    }
}

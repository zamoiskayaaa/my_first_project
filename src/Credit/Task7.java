package Credit;

public class Task7 {
    public static void main(String[] args) {
        int[] mass = new int[10];
        for (int i = 0; i < mass.length; i++) {
            mass[i]=(int)(Math.random()*9);
            System.out.print(mass[i] + " ");
        }
        System.out.println();
        int n = mass.length;
        for (int i = 0; i < n / 2; i++) {
            int temp = mass[n - i - 1];
            mass[n - i - 1] = mass[i];
            mass[i] = temp;
        }
        for (int i = 0; i < mass.length; i++) {
            System.out.print(mass[i] + " ");
        }
    }
}

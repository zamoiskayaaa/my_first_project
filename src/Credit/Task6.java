package Credit;

public class Task6 {
    public static void main(String[] args) {
        int[] mass = new int[10];
        int i, k, a = 0, b = 0, max = 0, min = 0;
        for (i = 0; i < mass.length; i++) {
            mass[i] = (int) (1 + Math.random() * 10);
            System.out.print(mass[i] + " ");
            if (max < mass[i]) {
                max = mass[i];
                a = i;
            }
        }
        min = max;
        for (k = 0; k < mass.length; k++) {
            if (min > mass[k]) {
                min = mass[k];
                b = k;
            }
        }
        System.out.println();
        System.out.println("Максимальный элемент массива " + max);
        System.out.println("Минимальный элемент массива " + min);
        System.out.println("Индекс максимального элемента " + a);
        System.out.println("Индекс минимального элемента " + b);
    }
}

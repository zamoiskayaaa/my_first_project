package Exam.Task9;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class File {
    public static void main(String[] args) throws IOException {
        try (BufferedWriter in1 = new BufferedWriter(new FileWriter("in1.txt"));
             FileWriter in2 = new FileWriter("in2.txt")) {
            for (int i = 0; i < 1000; i++) {
                in1.write(1 + (int) (Math.random() * 100000) + "\n");
                in2.write(1 + (int) (Math.random() * 100000) + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        ArrayList<Integer> num = new ArrayList<>();
        try {
            BufferedReader bufferedReader1 = new BufferedReader(new FileReader("in1.txt"));
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader("in2.txt"));
            String line;
            while ((line = bufferedReader1.readLine()) != null) {
                num.add(Integer.parseInt(line));
            }
            while ((line = bufferedReader2.readLine()) != null) {
                num.add(Integer.parseInt(line));
            }
        } catch (IOException | NumberFormatException e) {
            throw new RuntimeException(e);
        }
        Collections.sort(num);
        try (FileWriter out = new FileWriter("out.txt")) {
            for (Integer i : num) {
                out.write(i + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

package Exam;

public class Task4 {

    public static void main(String[] args) {
        int n = 7;
        int[][] mass = new int[n][n];
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                if (i == j || j == mass.length - 1 - i) {
                    mass[i][j] = 0;
                } else if (i < j && j < mass.length - 1 - i) {
                    mass[i][j] = (int) (1 + (Math.random() * 9));
                } else if (i > j && j < mass.length - 1 - i) {
                    mass[i][j] = (int) (-1 + (Math.random() * -9));
                } else if (i > j) {
                    mass[i][j] = (int) (1 + (Math.random() * 9));
                } else {
                    mass[i][j] = (int) (-1 + (Math.random() * -9));
                }
            }
        }
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                if (mass[i][j] < 0) {
                    System.out.print(mass[i][j] + " ");
                } else {
                    System.out.print(" " + mass[i][j] + " ");
                }
            }
            System.out.println();
        }
        int sum = 0;
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                sum = sum + mass[i][j];
            }
        }
        System.out.println("Сумма элементов массива " + sum);
        double middle=0;
        middle =(double) sum /(mass.length* mass.length);
        String more = "больше";
        String smaller = "меньше";
        if (middle > sum) {
            System.out.println("Cреднее арифметическое всех элементов " + more + " " + sum + ": " + middle);
        } else if (middle < sum) {
            System.out.println("Cреднее арифметическое всех элементов " + smaller + " " + sum + ": " + middle);
        } else {
            System.out.println("Cреднее арифметическое всех элементов равно " + sum + ": " + middle);
        }
    }
}

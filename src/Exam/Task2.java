package Exam;

public class Task2 {
    public static void main(String[] args) {
        int num = 0;
        for (int i = 000001; i < 100000; i++) {
            int i1 = i / 100000 % 10;
            int i2 = i / 10000 % 10;
            int i3 = i / 1000 % 10;
            int i4 = i / 100 % 10;
            int i5 = i / 10 % 10;
            int i6 = i % 10;
            if (i1 == 4 || i2 == 4 || i3 == 4 || i4 == 4 || i5 == 4 || i6 == 4) {
                num++;
            } else if (i1 == 1 && i2 == 3 || i2 == 1 && i3 == 3 || i3 == 1 && i4 == 3 || i4 == 1 && i5 == 3 || i5 == 1 && i6 == 3) {
                num++;
            }
        }
        System.out.println("Нужно исключить " + num + " номеров");
    }
}


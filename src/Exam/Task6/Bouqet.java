package Exam.Task6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Bouqet {
    public static void main(String[] args) {
        Flower lilacObj = new Flower(0.75, "Сирень", Color.Purple, 2);
        Flower liliesObj = new Flower(4.4, "Лилия", Color.Orange, 5);
        Flower orchidObj = new Flower(3.5, "Орхидея", Color.Yellow, 9);
        Flower roseObj = new Flower(2.5, "Роза", Color.Red, 3);
        Flower rose1Obj = new Flower(2.4, "Роза", Color.White, 3);
        Flower сhamomileObj = new Flower(2.1, "Ромашка", Color.White, 7);
        ArrayList<Flower> bouquet = new ArrayList<>();
        bouquet.add(lilacObj);
        bouquet.add(liliesObj);
        bouquet.add(orchidObj);
        bouquet.add(roseObj);
        bouquet.add(rose1Obj);
        bouquet.add(сhamomileObj);
        int maxFresh = 0;
        double totalPrice = 0;
        HashSet<Color> colorFlower = new HashSet<>();
        for (Flower flower: bouquet) {
            totalPrice += flower.getPrice();
            colorFlower.add(flower.getColor());
            if (flower.getFresh()>maxFresh) {
                maxFresh = flower.getFresh();
            }
        }
        System.out.println("Стоимость букета:  " + totalPrice);
        System.out.println("Цвета в букете:\n"+colorFlower);
        System.out.println("Через " + maxFresh + " дней весь букет завянет");
    }
}




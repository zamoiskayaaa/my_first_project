package Exam.Task6;

public class Flower {
    private double price;
    private String name;
    private Color color;
    private int fresh;

    public Flower(double price, String name, Color color, int fresh) {
        this.price = price;
        this.name = name;
        this.color = color;
        this.fresh = fresh;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public int getFresh() {
        return fresh;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", color=" + color +
                ", fresh=" + fresh +
                '}';
    }
}

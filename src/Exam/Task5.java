package Exam;

import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;


public class Task5 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println ("Пожалуйста, введите строку случайных символов:");
        String s=sc.nextLine();
        String s1 = s.toLowerCase(Locale.ROOT);
        String s2 = s1.trim(); // метод удаляет пробелы в начале строки и в конце(возможно это можно сделать с помощью регулярных выражений)
        String []ss=s2.split("\\s+");
        Set<String> sets=new HashSet<>();
        for(int i=0;i<ss.length;i++) {
            sets.add(ss[i]);
        }
        sc.close();
        System.out.println ("Количество уникальных слов в строке:" + sets.size ());
    }
}



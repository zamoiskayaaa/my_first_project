package Exam;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String[] minutes = {" минут ", " минута ", " минуты "};
        String[] seconds = {" секунд ", " секунда ", " секунды "};
        String[] hours = {" часов ", " час ", " часа "};
        String[] days = {" суток ", " сутки "};
        String[] weeks = {" недель ", " неделя ", " недели "};
        System.out.println("Введите число и я посчитаю сколько в нем недель, дней, часов, минут и секунд!");
        final Scanner scanner = new Scanner(System.in);
        long num = scanner.nextInt();
        long sec = num % 60;
        long min = num % 3600 / 60;
        long hour = num % 86400 / 3600; //в сутках 86400 секунд (60*60*24=86400)
        long day = num % 604800 / 86400; // в неделе 604800 секунд (86400*7=604800)
        long week = num / 604800;

        if (hour % 10 == 0 || hour % 5 == 0 || hour % 6 == 0 || hour % 7 == 0 || hour % 8 == 0 || hour % 9 == 0 || hour % 11 == 0 || hour % 12 == 0 || hour % 13 == 0 || hour % 14 == 0 || hour % 15 == 0 || hour % 16 == 0 || hour % 17 == 0 || hour % 18 == 0 || hour % 19 == 0) {
            System.out.print(hour + hours[0]);
        } else if (hour % 10 == 1) {
            System.out.print(hour + hours[1]);
        } else if (hour % 2 == 0 || hour % 3 == 0 || hour % 4 == 0) {
            System.out.print(hour + hours[2]);
        }
        if (min % 10 == 0 || min % 5 == 0 || min % 6 == 0 || min % 7 == 0 || min % 8 == 0 || min % 9 == 0 || min % 11 == 0 || min % 12 == 0 || min % 13 == 0 || min % 14 == 0 || min % 15 == 0 || min % 16 == 0 || min % 17 == 0 || min % 18 == 0 || min % 19 == 0) {
            System.out.print(min + minutes[0]);
        } else if (min % 10 == 1) {
            System.out.print(min + minutes[1]);
        } else if (min % 2 == 0 || min % 3 == 0 || min % 4 == 0) {
            System.out.print(min + minutes[2]);
        }
        if (sec % 10 == 0 || sec % 5 == 0 || sec % 6 == 0 || sec % 7 == 0 || sec % 8 == 0 || sec % 9 == 0 || sec % 11 == 0 || sec % 12 == 0 || sec % 13 == 0 || sec % 14 == 0 || sec % 15 == 0 || sec % 16 == 0 || sec % 17 == 0 || sec % 18 == 0 || sec % 19 == 0) {
            System.out.println(sec + seconds[0]);
        } else if (sec % 10 == 1) {
            System.out.println(sec + seconds[1]);
        } else if (sec % 2 == 0 || sec % 3 == 0 || sec % 4 == 0) {
            System.out.println(sec + seconds[2]);
        }
        if (num >= 86400) {
            if (week % 10 == 1) {
                System.out.print(week + weeks[1]);
            } else if (week % 10 == 0 || week % 5 == 0 || week % 6 == 0 || week % 7 == 0 || week % 8 == 0 || week % 9 == 0 || week % 11 == 0 || week % 12 == 0 || week % 13 == 0 || week % 14 == 0 || week % 15 == 0 || week % 16 == 0 || week % 17 == 0 || week % 18 == 0 || week % 19 == 0) {
                System.out.print(week + weeks[0]);
            } else if (week % 2 == 0 || week % 3 == 0 || week % 4 == 0) {
                System.out.print(week + weeks[2]);
            }
            if (day % 10 == 1) {
                System.out.print(day + days[1]);
            } else /*if (day%10==0 || day%5==0 || day%6==0 || day%7==0 || day%8==0 || day%9==0 || day%11==0 || day%12==0 || day%13==0|| day%14==0|| day%15==0|| day%16==0|| day%17==0|| day%18==0|| day%19==0)*/ {
                System.out.print(day + days[0]);
            }
            if (hour % 10 == 0 || hour % 5 == 0 || hour % 6 == 0 || hour % 7 == 0 || hour % 8 == 0 || hour % 9 == 0 || hour % 11 == 0 || hour % 12 == 0 || hour % 13 == 0 || hour % 14 == 0 || hour % 15 == 0 || hour % 16 == 0 || hour % 17 == 0 || hour % 18 == 0 || hour % 19 == 0) {
                System.out.print(hour + hours[0]);
            } else if (hour % 10 == 1) {
                System.out.print(hour + hours[1]);
            } else if (hour % 2 == 0 || hour % 3 == 0 || hour % 4 == 0) {
                System.out.print(hour + hours[2]);
            }
            if (min % 10 == 0 || min % 5 == 0 || min % 6 == 0 || min % 7 == 0 || min % 8 == 0 || min % 9 == 0 || min % 11 == 0 || min % 12 == 0 || min % 13 == 0 || min % 14 == 0 || min % 15 == 0 || min % 16 == 0 || min % 17 == 0 || min % 18 == 0 || min % 19 == 0) {
                System.out.print(min + minutes[0]);
            } else if (min % 10 == 1) {
                System.out.print(min + minutes[1]);
            } else if (min % 2 == 0 || min % 3 == 0 || min % 4 == 0) {
                System.out.print(min + minutes[2]);
            }
            if (sec % 10 == 0 || sec % 5 == 0 || sec % 6 == 0 || sec % 7 == 0 || sec % 8 == 0 || sec % 9 == 0 || sec % 11 == 0 || sec % 12 == 0 || sec % 13 == 0 || sec % 14 == 0 || sec % 15 == 0 || sec % 16 == 0 || sec % 17 == 0 || sec % 18 == 0 || sec % 19 == 0) {
                System.out.println(sec + seconds[0]);
            } else  if (sec % 10 == 1) {
                System.out.println(sec + seconds[1]);
            } else if (sec % 2 == 0 || sec % 3 == 0 || sec % 4 == 0) {
                System.out.println(min + minutes[2]);
            }
        }
    }
}
